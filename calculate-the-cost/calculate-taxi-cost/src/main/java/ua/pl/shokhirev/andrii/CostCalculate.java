package ua.pl.shokhirev.andrii;

import ua.pl.shokhirev.andrii.taxi.TaxiClasses;

public class CostCalculate {

    private static double priceBetween0And5 = 7;
    private static double priceBetween5And15 = 6;
    private static double priceOver15 = 5.50;
    private static double initialPrice = 40;
    private static double classicCoefficient = 1;
    private static double comfortCoefficient = 2;
    private static double coefficientForOutOfTownRides = 1.5;

    public double calculateTaxiCost(double kilometers, int numberOfClientRides, boolean isCityRide, TaxiClasses taxiClass) {
        double initialPrice = this.initialPrice;
        double surchargeForKilomiters = 0;
        surchargeForKilomiters = calculateSurchargeForKilometers(kilometers, surchargeForKilomiters, taxiClass);
        if (!isCityRide) {
            surchargeForKilomiters *= coefficientForOutOfTownRides;
        }
        return initialPrice + surchargeForKilomiters;
    }

    private static double calculateSurchargeForKilometers(double kilometers, double surchargeForKilometers, TaxiClasses taxiClass) {
        if (kilometers > 0 && kilometers <= 5) {
            surchargeForKilometers = kilometers * priceBetween0And5;
        } else if (kilometers > 5 && kilometers <= 15) {
            surchargeForKilometers = kilometers * priceBetween5And15;
        } else if (kilometers > 15) {
            surchargeForKilometers = kilometers * priceOver15;
        }
        surchargeForKilometers = taxiClassCorrection(kilometers, surchargeForKilometers, taxiClass);
        return surchargeForKilometers;
    }

    private static double taxiClassCorrection(double kilometers, double surchargeForKilometers, TaxiClasses taxiClass) {
        if (taxiClass.equals(TaxiClasses.CLASSIC)) {
            surchargeForKilometers += kilometers * classicCoefficient;
        } else if (taxiClass.equals(TaxiClasses.COMFORT)) {
            surchargeForKilometers += kilometers * comfortCoefficient;
        }
        return surchargeForKilometers;
    }
}
