package ua.pl.shokhirev.andrii.taxi;

public class Taxi {
    private TaxiClasses taxiClass;
    private String carNumber;

    public Taxi(TaxiClasses taxiClass, String carNumber) {
        this.taxiClass = taxiClass;
        this.carNumber = carNumber;
    }

    public TaxiClasses getTaxiClass() {
        return taxiClass;
    }

    public void setTaxiClass(TaxiClasses taxiClass) {
        this.taxiClass = taxiClass;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }
}
