package ua.pl.shokhirev.andrii.taxi;

public enum TaxiClasses {
    ECONOMY, CLASSIC, COMFORT
}
