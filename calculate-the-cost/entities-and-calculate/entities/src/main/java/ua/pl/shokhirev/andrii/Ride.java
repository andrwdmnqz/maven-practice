package ua.pl.shokhirev.andrii;

import ua.pl.shokhirev.andrii.taxi.Taxi;

public class Ride {
    private double rideDistance;
    private Client client;
    private boolean isCityRide;
    private Taxi taxiForRide;

    public Ride(Client client, boolean isCityRide, Taxi taxiForRide, double rideDistance) {
        this.client = client;
        this.isCityRide = isCityRide;
        this.taxiForRide = taxiForRide;
        this.rideDistance = rideDistance;
    }

    public double getRideDistance() {
        return rideDistance;
    }

    public void setRideDistance(double rideDistance) {
        this.rideDistance = rideDistance;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public boolean isCityRide() {
        return isCityRide;
    }

    public void setCityRide(boolean cityRide) {
        isCityRide = cityRide;
    }

    public Taxi getTaxiForRide() {
        return taxiForRide;
    }

    public void setTaxiForRide(Taxi taxiForRide) {
        this.taxiForRide = taxiForRide;
    }

    public void rideInfo() {
        CostCalculate costCalculate = new CostCalculate();
        double rideCost = costCalculate.calculateTaxiCost(rideDistance, client.getNumberOfRides(), isCityRide, taxiForRide.getTaxiClass());

        System.out.println("Client " + client.getName() + " has ordered taxi with number " +
                taxiForRide.getCarNumber() + " for " + rideDistance + "km ride for " + Math.ceil(rideCost) + " UAH");
    }
}
