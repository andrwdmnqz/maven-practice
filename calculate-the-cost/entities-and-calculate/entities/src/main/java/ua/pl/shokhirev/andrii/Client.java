package ua.pl.shokhirev.andrii;

public class Client {
    private String name;
    private int numberOfRides;

    public Client(String name, int countOfRides) {
        this.name = name;
        this.numberOfRides = countOfRides;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfRides() {
        return numberOfRides;
    }

    public void setNumberOfRides(int numberOfRides) {
        this.numberOfRides = numberOfRides;
    }
}
