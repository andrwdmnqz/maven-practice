package ua.pl.shokhirev.andrii;

import ua.pl.shokhirev.andrii.taxi.Taxi;
import ua.pl.shokhirev.andrii.taxi.TaxiClasses;

public class TaxiOrder {
    public static void main(String[] args) {
        Client clientMary = new Client("Mary", 0);
        Client clientJack = new Client("Jack", 3);

        Taxi firstTaxi = new Taxi(TaxiClasses.ECONOMY, "49DLD39");
        Taxi secondTaxi = new Taxi(TaxiClasses.COMFORT, "LDJISD842");

        Ride firstRide = new Ride(clientMary, true, firstTaxi, 10.3);
        Ride secondRide = new Ride(clientJack, false, secondTaxi, 18);

        firstRide.rideInfo();
        secondRide.rideInfo();
    }
}
